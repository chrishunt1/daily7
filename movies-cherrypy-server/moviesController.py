import cherrypy
import re, json
import time
from movies_library import _movie_database

class MovieController(object):

        def __init__(self, mdb=None, am=None):
                if mdb is None:
                        self.mdb = _movie_database()
                else:
                        self.mdb = mdb

                self.mdb.load_movies('movies.dat')

        def GET_KEY(self, movie_id):
                output = {'result':'success'}
                movie_id = int(movie_id)

                time.sleep(5)

                try:
                        movie = self.mdb.get_movie(movie_id)
                        if movie is not None:
                                output['id'] = movie_id
                                output['title'] = movie[0]
                                output['genres'] = movie[1]
                        else:
                                output['result'] = 'error'
                                output['message'] = 'movie not found'
                except Exception as ex:
                        output['result'] = 'error'
                        output['message'] = str(ex)

                return json.dumps(output)

        def PUT_KEY(self, movie_id):
                output = {'result':'success'}
                movie_id = int(movie_id)

                data = json.loads(cherrypy.request.body.read().decode('utf-8'))

                movie = list()
                movie.append(data['title'])
                movie.append(data['genres'])

                self.mdb.set_movie(movie_id, movie)

                return json.dumps(output)

        def DELETE_KEY(self, movie_id):
                #TODO
                output = {'result': 'success'}
                movie_id = int(movie_id)
                #data = json.loads(cherrypy.request.body.read().decode('utf-8'))
                #movie = list()
                #movie.append(data['title'])
                #movie.append(data['genres'])
                self.mdb.delete_movie(movie_id)

                return json.dumps(output)

        def GET_INDEX(self):
                output = {'result':'success'}
                output['movies'] = []

                try:
                        for mid in self.mdb.get_movies():
                                movie = self.mdb.get_movie(mid)
                                dmovie = {'id':mid, 'title':movie[0],
                                                'genres':movie[1]}
                                output['movies'].append(dmovie)
                except Exception as ex:
                        output['result'] = 'error'
                        output['message'] = str(ex)

                return json.dumps(output)

        def POST_INDEX(self):
                #TODO
                output = {'result' : 'success'}
                #output['movies'] = []
                data = json.loads(cherrypy.request.body.read().decode('utf-8'))
                i = max(self.mdb.get_movies()) + 1
                output['id'] = i
                print("i = ", i)
                #print(json.dumps(data))
                if len(data.keys()) > 1:
                    try:
                        title = data['title']
                        genre = data['genres']
                        movielist = list()
                        movielist.append(title)
                        movielist.append(genre)
                        self.mdb.set_movie(i, movielist)   

                    except Exception as ex:
                        print(ex)
                        output['result'] = 'error'
                        output['message'] = str(ex)

                return json.dumps(output)

        def DELETE_INDEX(self):
                #TODO
                output = {'result' : 'success'}
                #output['movies'] = []
                try:
                    midlist = list(self.mdb.get_movies())
                    for mid in midlist:
                        self.mdb.delete_movie(mid)

                except Exception as ex:
                    output['result'] = 'error'
                    output['message'] = str(ex)

                return json.dumps(output)
